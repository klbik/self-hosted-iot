
    module.exports = {
        flowFile: "flows.json",
        flowFilePretty: true,
        adminAuth: {
            type: "credentials",
            users: [
                {
                username: "user1",
                password: "$2a$10$3JCNrin/EZjrliuLK2zHfuhHTraWvCyWemH3epGiu5EbslPKFxgbW",
                permissions: "*",
                },
            ],
        },
        https: {
            key: require("fs").readFileSync("/certs/key.pem"),
            cert: require("fs").readFileSync("/certs/certificate.pem"),
        },
        requireHttps: true,
        uiPort: process.env.PORT || 1880,
        logging: {
            console: {
                level: "info",
                metrics: false,
                audit: false,
            },
        },
        exportGlobalContextKeys: false,
        externalModules: {
        },
        editorTheme: {
            palette: {

            },
            projects: {
                enabled: false,
                workflow: {
                    mode: "manual",
                },
            },
            codeEditor: {
                /** Select the text editor component used by the editor.
                 * Defaults to "ace", but can be set to "ace" or "monaco"
                 */
                lib: "monaco",
                options: {
                    theme: "vs", 
                },
            },
        },
        functionExternalModules: true,
        functionGlobalContext: {
            // os:require('os'),
        },
        debugMaxLength: 1000,
        mqttReconnectTime: 15000,
        serialReconnectTime: 15000,
    };
    